-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2020 at 07:54 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sig`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `id_kota` varchar(11) NOT NULL,
  `sTangan` int(3) NOT NULL,
  `apd` int(3) NOT NULL,
  `masker` int(3) NOT NULL,
  `kcmata` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `id_kota`, `sTangan`, `apd`, `masker`, `kcmata`) VALUES
(0, '0', 54, 10, 114, 23),
(1, '1', 43, 13, 231, 56),
(3, '3', 34, 23, 56, 123),
(4, '4', 123, 542, 32, 34);

-- --------------------------------------------------------

--
-- Table structure for table `koordinat`
--

CREATE TABLE `koordinat` (
  `id_kota` varchar(5) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `koordinat`
--

INSERT INTO `koordinat` (`id_kota`, `kota`, `latitude`, `longitude`) VALUES
('0', 'Kabupaten Semarang', '-7.240773', '110.467583'),
('1', 'Kabupaten Demak', '-6.906075', '110.641295'),
('3', 'Pekalongan', '-6.8958555', '109.6394838'),
('4', 'Pemalang', '-6.890316', '109.380686'),
('5', 'Kabupaten Cilacap', '-7.692774', '109.026278'),
('6', 'Kabupaten Banyumas', '-7.430469', '109.136288'),
('7', 'Kabupaten Purbalingga', '-7.387246', '109.364256');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `id` varchar(4) NOT NULL,
  `org` varchar(35) NOT NULL,
  `barang` varchar(35) NOT NULL,
  `jum` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`id`, `org`, `barang`, `jum`) VALUES
('0101', 'DINAS KESEHATAN KAB SEMARANG', 'SEPATU BOOT TOYOBO', 35),
('0102', 'DINAS KESEHATAN KAB SEMARANG', 'BAJU COVERALL KODAICHI', 17),
('0103', 'DINAS KESEHATAN KAB SEMARANG', 'SENSIFLEX 7', 27),
('0201', 'BPBD KAB SEMARANG', 'BAJU COVER ALL MICROPORUS', 13),
('0202', 'BPBD KAB SEMARANG', 'MASKER EARLOOP', 43),
('1101', 'DINAS KESEHATAN KAB DEMAK', 'MASKER EARLOOP', 63),
('1102', 'BPBD KAB DEMAK', 'masker bedah', 130);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `koordinat`
--
ALTER TABLE `koordinat`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
