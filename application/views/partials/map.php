<script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiamFsYW5rZWx1YXIiLCJhIjoiY2s4NDQ0emduMHNudjNybnozejJlcHdiNyJ9.dmJLmc37eArBnnGEMPOGjQ';
        var map = new mapboxgl.Map({
            center: [110.087134, -7.307074],
            zoom: 7.75,
            container: 'map',
            style: 'mapbox://styles/jalankeluar/ck84572al0ci21ikchwl35yd1'
        });
        
        map.addControl(new mapboxgl.NavigationControl());

        var geojson = {
            type: 'FeatureCollection',
            features: [
            <?php foreach ($koor as $tik){
            ?>
                {
                    type: 'Feature',
                    geometry: {
                                type: 'Point',
                                coordinates: [<?=$tik->longtitude?>, <?=$tik->latitude?>]
                                },
                    properties: {
                                title: '<center><?= $tik->OrgNama?>',
                                description: 'Total bantuan : <?= $tik->total?><br><center><a href="#services" class="btn btn-info" name="">Detail</a>'
                                }
                },
            <?php
            }
            ?>
            ]
        };

        // add markers to map
        geojson.features.forEach(function(marker) {

            // create a HTML element for each feature
            var el = document.createElement('div');
            el.className = 'marker';

            // make a marker for each feature and add to the map
            new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .setPopup(new mapboxgl.Popup({ offset: 10 }) // add popups
            .setHTML('<br><h6><b>' + marker.properties.title + '</b></h6><p style="font-size: 14px">' + marker.properties.description + '</p>'))
            .addTo(map);
        });
  </script>