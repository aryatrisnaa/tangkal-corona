<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php")?>
</head>

<body>

  <?php $this->load->view("partials/header.php")?>

  <section id="hero" class="clearfix">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">

        <div class="col-md-6 intro-img order-md-last order-first">
          <img src="<?php echo base_url('assets/img/apd.png')?>" alt="" class="img-fluid">
        </div>
      </div>

    </div>
  </section><!-- End Hero -->

<main id="main"><br> 
  <section class="section pt-0 position-relative" id="data">
    <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
		<div class="rounded-15 shadow bg-white">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12 mb-12 text-center p-4">
                    <h3 class="font-hotline font-weight-bold p-3 mb-3">Sebaran Alat Bantu Penanganan <span class="corona-text">COVID-19</span><br> Di Kabupaten X</h3>
                </div>
            </div>
            <div class="row justify-content-center mr-lg-5 mr-sm-5 ml-lg-5 ml-sm-5">
                <div class="col-lg-4 col-md-12 mb-3">
                    <div class="card shadow card-custom">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-center list-header-ungu font-status">
                                <i class="fa fa-circle fc-ungu"></i> Untuk RSUD blablabla
                            </li>
                            <li class="list-group-item text-center">
                                <h3>23.256 <sup>Total Bantuan</sup></h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 mb-3">
                    <div class="card shadow card-custom">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-center list-header-ungu font-status">
                                <i class="fa fa-circle fc-ungu"></i> Untuk RSUD bliblibli
                            </li>
                            <li class="list-group-item text-center">
                                <h3>2.256 <sup>Total Bantuan</sup></h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 mb-3">
                    <div class="card shadow card-custom">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-center list-header-ungu font-status">
                                <i class="fa fa-circle fc-ungu"></i> Untuk RSUD blablabla
                            </li>
                            <li class="list-group-item text-center">
                                <h3>23.256 <sup>Total Bantuan</sup></h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 mb-3">
                    <div class="card shadow card-custom">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-center list-header-ungu font-status">
                                <i class="fa fa-circle fc-ungu"></i> Untuk RSUD bliblibli
                            </li>
                            <li class="list-group-item text-center">
                                <h3>2.256 <sup>Total Bantuan</sup></h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 mb-5">
                    <div class="card shadow card-custom">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-center list-header-oren font-status">
                                <i class="fa fa-circle fc-orange"></i> Untuk Masyarakat
                            </li>
                            <li class="list-group-item text-center">
                                <h3>645 <sup>Total Bantuan</sup></h3>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center mr-lg-5 mr-sm-5 ml-lg-5 ml-sm-5">
                <div class="col-lg-12 col-md-12 col-sm-12 mr-3 ml-3">
                    <span class="fc-red ml-3"><b>RSUD blablabla</b></span>
                        <ul class="list-unstyled row ml-0" id="list_covid">
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 47 Alat Pelindung Diri (APD)</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 9 Sarung Tangan Karet</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 8 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 7 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 6 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 5 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 5 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 4 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 4 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 9 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 3 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 3 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 blablablablabalbal</li>
                        </ul>
                </div><br>
                <div class="col-lg-12 col-md-12 col-sm-12 mr-3 ml-3">
                    <span class="fc-green ml-3"><b>RSUD bliblibli</b></span>
                        <ul class="list-unstyled row ml-0">
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 8 lbiblblibilbi</li>
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 savnkalnkv.</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 3 savjlasnk</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 sacavs</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 2 asvnalsknfilsa kals</li>
                        </ul>
                </div><br>
                <div class="col-lg-12 col-md-12 col-sm-12 mr-3 ml-3">
                    <span class="fc-red ml-3"><b>RSUD blablabla</b></span>
                        <ul class="list-unstyled row ml-0" id="list_covid">
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 47 Alat Pelindung Diri (APD)</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 9 Sarung Tangan Karet</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 8 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 7 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 6 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 5 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 5 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 4 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 4 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 9 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 3 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 3 blablablablabalbal</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 blablablablabalbal</li>
                        </ul>
                </div><br>
                <div class="col-lg-12 col-md-12 col-sm-12 mr-3 ml-3">
                    <span class="fc-green ml-3"><b>RSUD bliblibli</b></span>
                        <ul class="list-unstyled row ml-0">
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 8 lbiblblibilbi</li>
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 savnkalnkv.</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 3 savjlasnk</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 sacavs</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 2 asvnalsknfilsa kals</li>
                        </ul>
                </div><br>
                <div class="col-lg-12 col-md-12 col-sm-12 mr-3 ml-3">
                    <span class="fc-green ml-3"><b>Masyarakat</b></span>
                        <ul class="list-unstyled row ml-0">
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 8 lbiblblibilbi</li>
							<li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 savnkalnkv.</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 3 savjlasnk</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 bg-gray"> 3 sacavs</li>
                            <li class="list-item col-lg-4 col-md-12 col-sm-12 py-2 "> 2 asvnalsknfilsa kals</li>
                        </ul>
                </div><br>
            </div>
        </div>    
	</div>
</section><br>
</main>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">
      <div class="container">

        <div class="row">

          <div class="col-lg-12">

            <div class="row">

              <div class="col-sm-8">

                <div class="footer-info">
                  <h3>BPKAD JATENG</h3>
                  <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
                </div>

              </div>

              <div class="col-sm-4">
                <div class="footer-links">
                  <h4>Contact Us</h4>
                  <p>
                    Jalan Taman Menteri Supeno Nomor 2 Semarang <br>
                    Kode Pos 50243 Telepon , Kota Semarang, Jawa Tengah<br><br>
                    <strong>Phone:</strong> 024-831117<br>
                    <strong>Email:</strong> bpkad@jatengprov.go.id<br>
                  </p>
                </div>
                <div class="social-links">
                  <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                  <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                  <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                  <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Aset BPKAD JATENG</strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Maps JS Leaflet -->
  <?php $this->load->view("partials/js.php")?>
</body>

<!-- <script>
    $('#dataTable').DataTable({
        "ordering": false
    });
</script> -->

</html>