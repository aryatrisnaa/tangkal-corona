<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php")?>
</head>

<body>

  <?php $this->load->view("partials/header.php")?>

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="clearfix">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">
        <div class="col-md-6 intro-info order-md-first order-last">
          <h2>Informasi Persebaran<br>Alat Bantu Penaganan <span>COVID-19</span></h2>
          <div>
            <a href="#about" class="btn-get-started scrollto">Lihat Peta</a>
          </div>
        </div>

        <div class="col-md-6 intro-img order-md-last order-first">
          <img src="<?php echo base_url('assets/img/apd.png')?>" alt="" class="img-fluid">
        </div>
      </div>

    </div>
  </section><!-- End Hero -->

  <main id="main">

  <!-- ======= Why Us Selection ======= -->
  <section id="why-us" class="why-us">
    <div class="container">
      <header style="margin-top:-15px;margin-bottom:-35px" class="section-header">
        <h3>Total Jumlah Bantuan yang Tersebar</h3>
      </header>
      <div class="row counters justify-content-center" style="margin-bottom:-35px">
        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up"><?php foreach($jumlah as $jml):?> <?php echo $jml->kel?><?php endforeach;?></span>
          <p>Barang</p>
        </div>
      </div>
    </div>
  </section><!-- End Why Us Section -->

    <!-- ======= Maps Section ======= -->
    <section id="about" class="about">
      <div style="margin-top:-58px"class="container col-sm-9">   
        <div id="map"></div>
      </div>
    </section><!-- End Map Section -->

    <!-- ======= Detail Section ======= -->
    <section id="services" class="services section-bg">
    <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
        <div class="rounded-15 shadow bg-white">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12 mb-12 row justify-content-center text-center p-5">
                    <h3 class="font-hotline font-weight-bold p-3 mb-3">Detail Informasi Bantuan Alat Pencegahan <span class="corona-text">COVID-19</span><br> di Jawa Tengah</h3>
                    <div class="table-responsive col-sm-10 ">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th width="50%" style="text-align:center">Kabupaten/Kota</th>
                            <th width="40%" style="text-align:center">Total Barang</th>
                            <th width="10%" style="text-align:center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($table as $s): ?>
                            <tr>
                              <td width="50%" style="text-align:justify">
                                  <?php echo $s->OrgNama ?>
                              </td>
                              <td width="40%" style="text-align:center">
                                  <?php echo $s->total ?>
                              </td>
                              <td width="10%" style="text-align:center">
                                  <button type="button" class="btn btn-info view_data" id="org"  onclick="detail('<?php echo $s->org ?>')" value =""> Detail </button>
                              </td>	
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section><!-- End Detail Section --> 
    <div class="modal fade" id="DetailModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="margin-top: -20px;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <!-- Place to print the fetched phone -->
            <div id="kend_result"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">
      <div class="container">

        <div class="row">

          <div class="col-lg-12">

            <div class="row">

              <div class="col-sm-8">

                <div class="footer-info">
                  <h3>BPKAD JATENG</h3>
                  <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
                </div>

              </div>

              <div class="col-sm-4">
                <div class="footer-links">
                  <h4>Contact Us</h4>
                  <p>
                    Jalan Taman Menteri Supeno Nomor 2 Semarang <br>
                    Kode Pos 50243 Telepon , Kota Semarang, Jawa Tengah<br><br>
                    <strong>Phone:</strong> 024-831117<br>
                    <strong>Email:</strong> bpkad@jatengprov.go.id<br>
                  </p>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Aset BPKAD JATENG</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Maps JS Leaflet -->
  <?php $this->load->view("partials/map.php")?>
  <?php $this->load->view("partials/js.php")?>
</body>

<script type="text/javascript">
     // Start jQuery function after page is loaded
        $(document).ready(function(){
         // Initiate DataTable function comes with plugin
        $('#dataTable').DataTable();
     });
          function detail(id)
        {
          var idlok = id;
          // var uuid = "'"+id+"'";
          $.ajax({
            // Path for controller function which fetches selected phone data
            url: "<?php echo base_url() ?>Home/detail",
            // Method of getting data
            method: "POST",
            // Data is sent to the server
            data: {idlok:idlok},
            // Callback function that is executed after data is successfully sent and recieved
            success: function(data){
              // Print the fetched data of the selected phone in the section called #kend_result 
              // within the Bootstrap modal
                $('#kend_result').html(data);
                // Display the Bootstrap modal
                $('#DetailModal').modal('show');
            }
          });
        }  
    </script>
<script>
    $('#dataTable').DataTable({
        "ordering": false
    });
</script>

</html>