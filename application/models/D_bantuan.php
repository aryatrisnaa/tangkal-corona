<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class D_bantuan extends CI_Model {
 
    public function __construct()
	{
		$this->load->database();
	}
    
    public function getKab($idlok) //done sesuai
	{
		$res2 = $this->db->query("SELECT a.Dikeluarkan, c.BarangNama, c.Satuan, d.OrgNama FROM dBCovid19Posko.dbo.PengeluaranDet a 
								LEFT JOIN dBCovid19Posko.dbo.Pengeluaran b on a.NoKeluar=b.NoKeluar
								LEFT JOIN dBCovid19.dbo.Barang c on a.Barang=c.Barang
								LEFT JOIN dBCovid19Posko.dbo.OrgPenerima d on b.Org=d.Org
								where LEFT(b.Org,2)='$idlok'")->result();
		return $res2;
	}

	public function sumBrg()//done
	{
		$sum = $this->db->query("SELECT SUM(Dikeluarkan) kel from dBCovid19Posko.dbo.PengeluaranDet")->result();
		return $sum;
	}

	public function judulDet($idlok) //done sesuai
	{
		$jud = $this->db->query("SELECT d.org, e.OrgNama, SUM(a.Dikeluarkan) total from dBCovid19Posko.dbo.PengeluaranDet a
								LEFT JOIN dBCovid19Posko.dbo.Pengeluaran b on a.NoKeluar=b.NoKeluar
								LEFT JOIN dBCovid19Posko.dbo.OrgPenerima c on b.Org=c.Org
								LEFT JOIN dBCovid19.dbo.LokasiMap d on left(c.Org,2)=d.org 
								LEFT JOIN dBCovid19Posko.dbo.OrgPenerima e on d.org=left(e.Org,2) and right(e.Org,2)='00'
								where d.org='$idlok'
								group by e.OrgNama, d.org")->result();
		return $jud;
	}

	public function koordinat() //done sesuai record transaksi
	{
		$kor = $this->db->query("SELECT d.latitude, d.longtitude, e.OrgNama, SUM(a.Dikeluarkan) total from dBCovid19Posko.dbo.PengeluaranDet a
								LEFT JOIN dBCovid19Posko.dbo.Pengeluaran b on a.NoKeluar=b.NoKeluar
								LEFT JOIN dBCovid19Posko.dbo.OrgPenerima c on b.Org=c.Org
								LEFT JOIN dBCovid19.dbo.LokasiMap d on left(c.Org,2)=d.org 
								LEFT JOIN dBCovid19Posko.dbo.OrgPenerima e on d.org=left(e.Org,2) and right(e.Org,2)='00'
								group by d.latitude, d.longtitude, e.OrgNama")->result();
		return $kor;
	}

	public function detTable() //done sesuai
	{
		$lok = $this->db->query("SELECT d.org, e.OrgNama, SUM(a.Dikeluarkan) total from dBCovid19Posko.dbo.PengeluaranDet a
								LEFT JOIN dBCovid19Posko.dbo.Pengeluaran b on a.NoKeluar=b.NoKeluar
								LEFT JOIN dBCovid19Posko.dbo.OrgPenerima c on b.Org=c.Org
								LEFT JOIN dBCovid19.dbo.LokasiMap d on left(c.Org,2)=d.org 
								LEFT JOIN dBCovid19Posko.dbo.OrgPenerima e on d.org=left(e.Org,2) and right(e.Org,2)='00' 
								group by e.OrgNama, d.org")->result();
		return $lok;
	}

}