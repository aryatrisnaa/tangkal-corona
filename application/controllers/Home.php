<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('D_bantuan'); // Auto load model M_Index pada fungsi
    }

	public function index()
	{
        $data["table"]=$this->D_bantuan->detTable();
        $data["koor"]=$this->D_bantuan->koordinat();
        $data["jumlah"]=$this->D_bantuan->sumBrg();
        $this->load->view('home', $data);
    }

    public function detail()
	{
        $idlok = $this->input->post('idlok');
        if(isset($idlok)){
            $records = $this->D_bantuan->getKab($idlok);
            $judul = $this->D_bantuan->judulDet($idlok);
            $output = '';
            $output .= '      
            <section class="section pt-0 position-relative" id="data">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 col-md-12 mb-12 text-center p-4">
                        ';
                        foreach ($judul as $jud){
                        $output .= '
                            <h4 class="font-hotline font-weight-bold p-3 mb-3">Sebaran Bantuan Alat Penanganan <span class="corona-text">COVID-19</span><br> Di Kabupaten '.$jud->OrgNama.'</h4>
                            <h5> Jumlah : <b>'.$jud->total.'</b> bantuan </h5>
                            ';
                        }
                        $output .= '
                        </div>
                        <div class="col-lg-11">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="35%">Untuk :</th>
                                        <th width="50%">Barang :</th>
                                        <th width="15%" style="text-align:center">Jumlah :</th>
                                    </tr>
                                </thead>
                                <tbody>';
                        foreach ($records as $row){
                        $output .= ' <tr>
                                        <td width="35%" style="text-align:justify; font-size: 13px">
                                            <li>'.$row->OrgNama.'</li>
                                        </td>
                                        <td width="50%" style="text-align:justify; font-size: 13px">
                                            '.$row->BarangNama.'
                                        </td>
                                        <td width="15%" style="text-align:center; font-size: 13px">
                                            '.$row->Dikeluarkan.' '.$row->Satuan.'
                                        </td>	
                                    </tr>';
                                }
                    $output .= '</tbody>
                            </table>
                        </div>
                    </div>
            </section>';
                  
               echo $output;
           }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Data Tidak Ditemukan'.'</li></ul></center>';
           }
	}
    
}
