<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('D_bantuan'); // Auto load model M_Index pada fungsi
    }

	public function index()
	{

        $this->load->view('data_bantuan');
    }

}
